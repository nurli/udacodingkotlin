package com.example.renofinsa.animalkotlin.Activity.MainMenu

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import com.example.renofinsa.animalkotlin.Activity.MainMenu.AnimalsActivity.*
import com.example.renofinsa.animalkotlin.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class ListHewan : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO : user RelativeLyaout in parent
        relativeLayout {
            backgroundColor = ContextCompat.getColor(
                ctx,
                R.color.colorBgListHewan
            )

            //TODO: use Scrollview
            scrollView {

                //TODO: use LinearLayout for Buttons
                linearLayout {
                    orientation = LinearLayout.VERTICAL

                    //TODO: create elephant Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Elephant"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Elephant>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Bird Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Bird"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Bird>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Donkey Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Donkey"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Donkey>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Frog Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Frog"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Frog>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Horse Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Horse"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Horse>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Bee Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Bee"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Bee>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Dolphins Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Dolphins"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Dolphins>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Cow Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Cow"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Cow>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Wolf Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Wolf"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Wolf>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }

                    //TODO: create Lion Button
                    button {
                        backgroundResource = R.drawable.btn_round
                        text = "Lion"
                        textColorResource = R.color.colorWhite
                        onClick {
                            startActivity(intentFor<Lion>())
                        }
                    }.lparams {
                        margin = dip(8)
                        width = matchParent
                        height = wrapContent
                    }
                }.lparams {
                    width = dip(250)
                    height = dip(400)
                    marginStart = dip(8)
                    leftMargin = dip(8)
                    marginEnd = dip(8)
                    rightMargin = dip(10)
                    bottomMargin = dip(153)
                }

            }.lparams {
                width = wrapContent
                height = dip(450)
                centerInParent()
                alignParentTop()
                topMargin = dip(57)
                leftMargin = dip(57)
                marginStart = dip(57)
            }

            //TODO: create back Button with imageView
            imageView {
                backgroundResource = R.drawable.back_button
                onClick {
                    onBackPressed()
                }
            }.lparams {
                margin = dip(16)
                alignParentRight()
                alignParentBottom()
                width = dip(50)
                height = dip(50)
            }
        }
    }

}
package com.example.renofinsa.animalkotlin.Activity.MainMenu

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.renofinsa.animalkotlin.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        //TODO : user RelativeLyaout in parent
        relativeLayout {
            backgroundColor = ContextCompat.getColor(
                ctx,
                R.color.colorBackgroundMainActivity
            )

            //TODO : set Image Background
            imageView {
                scaleType = ImageView.ScaleType.FIT_XY
                backgroundResource = R.drawable.bg_animals
            }.lparams {
                centerInParent()
            }

            //TODO : use Linearlayout for button action
            linearLayout {
                orientation = LinearLayout.VERTICAL

                //TODO : create Animal Button
                button {
                    backgroundResource = R.drawable.btn_round
                    text = "Pilih Hewan"
                    textColorResource = R.color.colorWhite
                    onClick {
                        startActivity(intentFor<ListHewan>())
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }

                //TODO : create Setting Button
                button {
                    backgroundResource = R.drawable.btn_round
                    text = "Pengaturan"
                    textColorResource = R.color.colorWhite
                    onClick {
                        startActivity(intentFor<PengaturanSuara>())
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }

                //TODO : create History Button
                button {
                    backgroundResource = R.drawable.btn_round
                    text = "Histori"
                    textColorResource = R.color.colorWhite
                    onClick {
                        startActivity(intentFor<Histori>())
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }

                //TODO : create Quit Button
                button {
                    backgroundResource = R.drawable.btn_round
                    text = "Keluar"
                    textColorResource = R.color.colorWhite
                    onClick {
                        finish()
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }

            }.lparams {
                width = dip(180)
                height = wrapContent
                marginEnd = dip(21)
                rightMargin = dip(21)
                bottomMargin = dip(110)
                alignParentEnd()
                alignParentRight()
                alignParentBottom()
            }
        }
    }
}

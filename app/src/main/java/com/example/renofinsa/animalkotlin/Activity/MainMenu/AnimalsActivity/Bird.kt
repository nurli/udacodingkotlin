package com.example.renofinsa.animalkotlin.Activity.MainMenu.AnimalsActivity

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ContextThemeWrapper
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.renofinsa.animalkotlin.R
import com.example.renofinsa.animalkotlin.R.style.buttonBack
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.style
import org.jetbrains.anko.sdk25.coroutines.onClick

class Bird : AppCompatActivity(){

    var mediaPlayer: MediaPlayer? = null
    var REQ_CODE_SPEECH_INPUT = 1000;
    var label: TextView? = null
    var image: ImageView? = null
    var back: ImageView? = null

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO: user relativeLayout for parent
        relativeLayout {
            backgroundColor = ContextCompat.getColor(
                ctx, R.color.colorBgElephant
            )

            //TODO: use linearLayout for subparent
            linearLayout {
                orientation = LinearLayout.VERTICAL

                //TODO: create imageView for Bird
                image = imageView {
                    backgroundResource = R.drawable.burung
                }.lparams {
                    gravity = Gravity.CENTER
                    width = dip(200)
                    height = dip(200)
                }

                //TODO: create textView for result
                label = textView {
                    text = "What animal is this ?"
                    textSize = 25f
                }.lparams {
                    topMargin = dip(16)
                    gravity = Gravity.CENTER
                }

                //TODO: create button to speak hoo is the animal
                button {
                    text = "Speak"
                    gravity = Gravity.CENTER
                    backgroundResource = R.drawable.btn_round2
                    textColorResource = R.color.colorWhite
                    onClick {
                        promptSpeechInput()
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }
            }.lparams {
                centerInParent()
            }

            //TODO: create back Button with imageView

            imageView {
                backgroundResource = R.drawable.back_button
                onClick {
                    onBackPressed()
                }
            }.lparams {
                margin = dip(16)
                alignParentRight()
                alignParentBottom()
                width = dip(50)
                height = dip(50)
            }
        }
    }

    private fun promptSpeechInput() {

    }
}
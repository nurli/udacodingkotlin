package com.example.renofinsa.animalkotlin.Activity.MainMenu.AnimalsActivity

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.renofinsa.animalkotlin.R
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*

class Elephant: AppCompatActivity() {

    var mediaPlayer: MediaPlayer? = null
    var REQ_CODE_SPEECH_INPUT = 1000;
    var label: TextView? = null
    var image: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO: user relativeLayout for parent
        relativeLayout {
            backgroundColor = ContextCompat.getColor(
                ctx, R.color.colorBgElephant
            )

            //TODO: use linearLayout for subparent
            linearLayout {
                orientation = LinearLayout.VERTICAL

                //TODO: create imageView for Elephant
                image = imageView {
                    backgroundResource = R.drawable.gajah
                }.lparams {
                    gravity = Gravity.CENTER
                    width = dip(200)
                    height = dip(200)
                }

                //TODO: create textView for result
                label = textView {
                    text = "What animal is this ?"
                    textSize = 25f
                }.lparams {
                    topMargin = dip(16)
                    gravity = Gravity.CENTER
                }

                //TODO: create button to speak hoo is the animal
                button {
                    text = "Speak"
                    gravity = Gravity.CENTER
                    backgroundResource = R.drawable.btn_round2
                    textColorResource = R.color.colorWhite
                    onClick {
                        promptSpeechInput()
                    }
                }.lparams {
                    margin = dip(8)
                    width = matchParent
                    height = wrapContent
                }
            }.lparams {
                centerInParent()
            }

            //TODO: create back Button with imageView
            imageView {
                backgroundResource = R.drawable.back_button
                onClick {
                    onBackPressed()
                }
            }.lparams {
                margin = dip(16)
                alignParentRight()
                alignParentBottom()
                width = dip(50)
                height = dip(50)
            }
        }
    }

    //TODO: create function promptSpeechInput()
    private fun promptSpeechInput() {
        intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say Something ?")

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                "Sorry! Your device doesn't support speech input",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    //TODO: use method onActivityResult()
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == RESULT_OK && null != data) {

                    val result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    //labelView.setText(result.get(0));
                    setResult(result[0])

                }
            }
        }
    }

    //TODO: create function setResult()
    @SuppressLint("ResourceAsColor")
    private fun setResult(result: String) {
        var result = result
        label?.setText(result)
        result = result.toLowerCase()

        if ("elephant".startsWith(result)) {
            mediaPlayer = MediaPlayer()
            play("gajah.wav")
            image?.setBackgroundResource(R.drawable.gajah)
            label!!.textColorResource = R.color.colorBlack

        } else {
            label!!.textColorResource = R.color.colorRed
            label?.setText("You are wrong")
            image?.setBackgroundResource(R.drawable.failure)
            play("fail-trumpet.wav")
//            onBackPressed()
        }
    }

    //TODO: create function play()
    private fun play(filename: String) {
        try {
            val assetFileDescriptor = this.assets.openFd(filename)
            mediaPlayer?.setDataSource(
                assetFileDescriptor.fileDescriptor,
                assetFileDescriptor.startOffset,
                assetFileDescriptor.length
            )
            assetFileDescriptor.close()
            mediaPlayer?.prepare()
            mediaPlayer?.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}




